package com.mfs.client.gimac.util;

public enum MFSGimacCodes {
	
	/*VALIDATION_ERROR("ER206","Validation Error"),
	ER202("ER202","Exception while getting TransactionLog by extrTrId."),
	ER203("ER203","Exception while processing your request, Please try again later."),
	ER204("ER204","Exception while save data"),
	ER205("ER205","Exception while update"),
	ER201("ER201","MFS System Error"),
	ER207("ER207","Request can not be null"),
	ER208("ER208","MSISDN field is required"),
	ER211("ER211","TrId field is required"),
	ER212("ER212","Duplicate extrId field i.e External Transaction Id"),
	ER213("ER213","Null response"),
	ER220("ER220","Exception while getting KycEnquiry by msisdn"),
	ER221("ER221","Transaction does not exist"),
	S200("200","Transaction Success"),
	ER401("ER401","Transaction Fail"),
	TR200("TR200","Transaction Init"),*/
	
	S200("200","Transaction Success"),
	ER400("ER400","Transaction rejected"),
	ER401("ER401","Bad or expired access token"),
	ER403("ER403","Access forbidden"),
	ER500("ER500","internal error"),
	ER206("206","Transaction doesn't exist against ExTrId"),
	ER214("ER214","Mobile number is not register");
	
    private String code;
	private String message;

	private MFSGimacCodes(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}


}
