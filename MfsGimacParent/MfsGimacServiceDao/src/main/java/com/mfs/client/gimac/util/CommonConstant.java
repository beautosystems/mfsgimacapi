package com.mfs.client.gimac.util;

public class CommonConstant {

	public static final String BASE_URL = "base_url";
	public static final String CONTENT_TYPE = "Content-type";
	public static final String APPLICATION_JSON = "application/json";
	public static final String APPLICATION_XML = "application/xml";
	public static final String TEXT_XML = "text/xml";

	public static final String PORT = "port";
	public static final String HTTP_METHOD = "httpmethod";
	public static final String ISHTTPS = "ishttps";

	public static final String USERNAME = "user_name";
	public static final String PASSWORD = "password";
	
	
}
