package com.mfs.client.gimac.util;

public class CommonValidations {
	public boolean validateStringValues(String reqParamValue) {
		boolean validateResult = false;
		String request = reqParamValue.trim();
		if (request == null || request.equals(""))
			validateResult = true;

		return validateResult;
	}

	public boolean validateAmountValues(Double amount) {
		boolean validateResult = false;
		if (amount == null)
			validateResult = true;

		return validateResult;
	}
}
