package com.mfs.client.gimac.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GimacController {

	@GetMapping(value = "/test")
	public String  test() {
		
		String s = "Hi";
		System.out.println(s);

		return s;
		
	}
	
	
}
